﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum angleMeterType
{
    Vertical, Horizontal
}

public class AngleMeter : MonoBehaviour {
    public angleMeterType type;

    public Transform anglePointer;
    public Transform angleBars;

    public Transform spaceShip;

    private float highAngle;
    private float lowAngle;
    private float currentAngle;

    private float highPointerPosition;
    private float lowPointerPosition;

    // Use this for initialization
    private void Awake()
    {
        int childrenNumber = angleBars.childCount;

        highAngle = spaceShip.GetComponent<SpaceshipTurnController>().tiltAngle;
        lowAngle = -spaceShip.GetComponent<SpaceshipTurnController>().tiltAngle;

        // NOTE: For the vertical angle meter, child 0 is the top bar, child (n-2) is the bottom bar, child (n-1) is the pointer
        // NOTE: For the horizontal angle meter, child 0 is the most right bar, child (n-2) is the most left bar, child (n-1) is the pointer
        highPointerPosition = angleBars.GetChild(0).localPosition.y;
        lowPointerPosition = angleBars.GetChild(childrenNumber - 2).localPosition.y;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch(type)
        {
            case angleMeterType.Vertical:
                currentAngle = spaceShip.eulerAngles.x > 180 ? spaceShip.eulerAngles.x - 360f : spaceShip.eulerAngles.x;
                break;
            case angleMeterType.Horizontal:
                currentAngle = spaceShip.eulerAngles.z > 180 ? spaceShip.eulerAngles.z - 360f : spaceShip.eulerAngles.z;
                break;
            default:
                break;
        }
        MoveAnglePointer(currentAngle);
	}

    //==================================================
    void MoveAnglePointer(float angle)
    {
        float angleInMeterScale = (angle - lowAngle) / (highAngle - lowAngle) * (highPointerPosition - lowPointerPosition) + lowPointerPosition;
        //Debug.Log(angleInMeterScale);
        anglePointer.localPosition = new Vector3(anglePointer.localPosition.x, angleInMeterScale, anglePointer.localPosition.z);
    }
}
