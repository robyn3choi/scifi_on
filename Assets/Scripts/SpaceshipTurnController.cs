﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipTurnController : MonoBehaviour {
    public ControllerManager controllerManager;
    public AutoPark autoPark;

    public float tiltSpeed;
    public float rotationSpeed;

    public float tiltAngle;

    private Vector3 originalRotation;
    
    // Use this for initialization
    void Start () {
        originalRotation = transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
        if (autoPark.parking || autoPark.parked)
            return;
        TurnLeftRight();
        //if (!controllerManager.JoystickTrigger)
        TurnUpDown();
        //Debug.Log(controllerManager.JoystickTrigger);

        KeyboardControls();
	}

    void KeyboardControls() {
        if (Input.GetKey("d")) {
            HorizontallyTilt(15);
            Rotate(15);
        }

        if (Input.GetKey("a")) {
            HorizontallyTilt(-15);
            Rotate(-15);
        }
    }

    //============================================================
    // Function: Tilt the Spaceship horizontally if the wheel rotates
    //============================================================
    void HorizontallyTilt(float wheelRotation)
    {
        Vector3 currentTiltAngle = new Vector3(
            transform.eulerAngles.x, 
            transform.eulerAngles.y, 
            ClampAngle(Mathf.LerpAngle(transform.eulerAngles.z, originalRotation.z + wheelRotation, Time.deltaTime * tiltSpeed), -tiltAngle, tiltAngle));

        transform.eulerAngles = currentTiltAngle;
    }

    //============================================================
    // Function: Rotate the Spaceship if the wheel rotates
    //============================================================
    void Rotate(float wheelRotation)
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + wheelRotation * Time.deltaTime * rotationSpeed, transform.eulerAngles.z);
    }

    //============================================================
    // Function: Turn the Spaceship left or right, combine tilting and rotating
    //============================================================

    void TurnLeftRight()
    {
        float horizontalJoystickInput = controllerManager.joystickHorizontal * 120;
        //Debug.Log(horizontalJoystickInput);
        HorizontallyTilt(horizontalJoystickInput);
        Rotate(horizontalJoystickInput);
    }

    //============================================================
    // Function: Turn the Spaceship up and down
    //============================================================
    void TurnUpDown()
    {
        float joyStickInput = controllerManager.joystickVertical;
        //Debug.Log(joyStickInput);

        Vector3 currentTiltAngle = new Vector3(
            ClampAngle(Mathf.LerpAngle(transform.eulerAngles.x, originalRotation.x + joyStickInput * 90, Time.deltaTime * tiltSpeed), -tiltAngle, tiltAngle),
            transform.eulerAngles.y,
            transform.eulerAngles.z
            );

        transform.eulerAngles = currentTiltAngle;
    }

    // Clamp an Eugle Angle
    float ClampAngle(float angle, float min, float max)
    {
        if (angle > 180) angle = angle - 360;
        angle = Mathf.Clamp(angle, min, max);

        return angle;
    }
}
