﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSlowly : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    private float i;
    public float RotationSpeed;
	// Update is called once per frame
	void Update () {
		transform.eulerAngles = new Vector3(0, i, 0);
	    i += Time.deltaTime * RotationSpeed;
	}
}
