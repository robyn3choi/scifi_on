﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    #region Events
    public delegate void XButtonDownEvent();
    public delegate void YButtonDownEvent();
    public delegate void ZButtonDownEvent();
    public delegate void AButtonDownEvent();
    public delegate void BButtonDownEvent();
    public delegate void CButtonDownEvent();
    public delegate void LTriggerDownEvent();
    public delegate void RTriggerDownEvent();

    public delegate void XButtonHoldEvent();
    public delegate void YButtonHoldEvent();
    public delegate void ZButtonHoldEvent();
    public delegate void AButtonHoldEvent();
    public delegate void BButtonHoldEvent();
    public delegate void CButtonHoldEvent();
    public delegate void LTriggerHoldEvent();
    public delegate void RTriggerHoldEvent();

    public delegate void XButtonUpEvent();
    public delegate void YButtonUpEvent();
    public delegate void ZButtonUpEvent();
    public delegate void AButtonUpEvent();
    public delegate void BButtonUpEvent();
    public delegate void CButtonUpEvent();
    public delegate void LTriggerUpEvent();
    public delegate void RTriggerUpEvent();

    public event XButtonDownEvent XButtonDown;
    public event YButtonDownEvent YButtonDown;
    public event ZButtonDownEvent ZButtonDown;
    public event AButtonDownEvent AButtonDown;
    public event BButtonDownEvent BButtonDown;
    public event CButtonDownEvent CButtonDown;
    public event LTriggerDownEvent LTriggerDown;
    public event RTriggerDownEvent RTriggerDown;

    public event XButtonHoldEvent XButtonHold;
    public event YButtonHoldEvent YButtonHold;
    public event ZButtonHoldEvent ZButtonHold;
    public event AButtonHoldEvent AButtonHold;
    public event BButtonHoldEvent BButtonHold;
    public event CButtonHoldEvent CButtonHold;
    public event LTriggerHoldEvent LTriggerHold;
    public event RTriggerHoldEvent RTriggerHold;

    public event XButtonUpEvent XButtonUp;
    public event YButtonUpEvent YButtonUp;
    public event ZButtonUpEvent ZButtonUp;
    public event AButtonUpEvent AButtonUp;
    public event BButtonUpEvent BButtonUp;
    public event CButtonUpEvent CButtonUp;
    public event LTriggerUpEvent LTriggerUp;
    public event RTriggerUpEvent RTriggerUp;
    #endregion

    #region Properties

    public bool JoystickTrigger
    {
        get { return joystickTrigger; }
        set
        {
            joystickTrigger = value;
        }
    }

    public bool XButton
    {
        get { return xButton; }
        set
        {
            if (!xButton && value)
            {
                if (XButtonDown != null) XButtonDown();
            }
            else if (xButton && !value)
            {
                if (XButtonUp != null) XButtonUp();
            }
            else if (xButton && value)
                if (XButtonHold != null) XButtonHold();
            xButton = value;
        }
    }
    public bool YButton
    {
        get { return yButton; }
        set
        {
            if (!yButton && value)
            {
                if (YButtonDown != null) YButtonDown();
            }
            else if (yButton && !value)
            {
                if (YButtonUp != null) YButtonUp();
            }
            else if (yButton && value)
                if (YButtonHold != null) YButtonHold();
            yButton = value;
        }
    }
    public bool ZButton
    {
        get { return zButton; }
        set
        {
            if (!zButton && value)
            {
                if (ZButtonDown != null) ZButtonDown();
            }
            else if (zButton && !value)
            {
                if (ZButtonUp != null) ZButtonUp();
            }
            else if (zButton && value)
                if (ZButtonHold != null) ZButtonHold();
            zButton = value;
        }
    }
    public bool AButton
    {
        get { return aButton; }
        set
        {
            if (!aButton && value)
            {
                if (AButtonDown != null) AButtonDown();
            }
            else if (aButton && !value)
            {
                if (AButtonUp != null) AButtonUp();
            }
            else if (aButton && value)
                if (AButtonHold != null) AButtonHold();
            aButton = value;
        }
    }
    public bool BButton
    {
        get { return bButton; }
        set
        {
            if (!bButton && value)
            {
                if (BButtonDown != null) BButtonDown();
            }
            else if (bButton && !value)
            {
                if (BButtonUp != null) BButtonUp();
            }
            else if (bButton && value)
                if (BButtonHold != null) BButtonHold();
            bButton = value;
        }
    }
    public bool CButton
    {
        get { return cButton; }
        set
        {
            if (!cButton && value)
            {
                if (CButtonDown != null) CButtonDown();
            }
            else if (cButton && !value)
            {
                if (CButtonUp != null) CButtonUp();
            }
            else if (cButton && value)
                if (CButtonHold != null) CButtonHold();
            cButton = value;
        }
    }

    public bool LTrigger
    {
        get { return lTrigger; }
        set
        {
            if (!lTrigger && value)
            {
                if (LTriggerDown != null) LTriggerDown();
            }
            else if (lTrigger && !value)
            {
                if (LTriggerUp != null) LTriggerUp();
            }
            else if (lTrigger && value)
                if (LTriggerHold != null) LTriggerHold();
            lTrigger = value;
        }
    }
    public bool RTrigger
    {
        get { return rTrigger; }
        set
        {
            if (!rTrigger && value)
            {
                if (RTriggerDown != null) RTriggerDown();
            }
            else if (rTrigger && !value)
            {
                if (RTriggerUp != null) RTriggerUp();
            }
            else if (rTrigger && value)
                if (RTriggerHold != null) RTriggerHold();
            rTrigger = value;
        }
    }
    #endregion

    public float wheelRotation;

    public float leftPedal;
    public float rightPedal;

	public float joystickVertical;
    public float joystickHorizontal;

    private bool joystickTrigger;
    private bool xButton;
    private bool yButton;
    private bool zButton;
    private bool aButton;
    private bool bButton;
    private bool cButton;
    private bool lTrigger;
    private bool rTrigger;
    private bool flag;
    private bool isPaused;
    private bool waitForControllerInput;
    private bool waitForJoystickInput;
    private float[] initialInput;
    // Use this for initialization
    void Start()
    {
        isPaused = false;
        initialInput = new[] { Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), Input.GetAxis("HorizontalJoystick") };
        waitForControllerInput = true;
        waitForJoystickInput = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPaused)
            return;

        if (!waitForJoystickInput)
        {
            ReadJoystick();
        }
        else if (initialInput[0] != Input.GetAxis("Vertical") || initialInput[2] != Input.GetAxis("HorizontalJoystick"))
            waitForJoystickInput = false;

        if (!waitForControllerInput)
        {              
            ReadWheel();
        }
        else if (initialInput[1] != Input.GetAxis("Horizontal"))
        {
            waitForControllerInput = false;
        }

        ReadButtons();
    }

    void OnApplicationFocus(bool focus)
    {
        isPaused = !focus;
        waitForControllerInput = true;
        waitForJoystickInput = true;
    }

    void LateUpdate()
    {
        //Avoiding the input starting at 60 degrees for the wheel and 1 for both pedals (instead of 0)
        if (flag)
            return;
        ResetValues();
        flag = true;

    }

    void ReadButtons()
    {
        //Debug.Log(JoystickTrigger);
        JoystickTrigger = Input.GetAxis("JoystickFire1") == 1;

        AButton = Input.GetAxis("Fire2") == 1;
        BButton = Input.GetAxis("Fire3") == 1;
        CButton = Input.GetAxis("Fire4") == 1;
        XButton = Input.GetAxis("Fire5") == 1;
        YButton = Input.GetAxis("Fire6") == 1;
        ZButton = Input.GetAxis("Fire7") == 1;
        LTrigger = Input.GetAxis("Fire8") == 1;
        RTrigger = Input.GetAxis("Fire9") == 1;
    }

    void ReadPedals()
    {
        leftPedal = Math.Abs(Input.GetAxis("ZAxis") - 1) / 2f;
        rightPedal = Math.Abs(Input.GetAxis("Vertical") + 1) / 2f;
    }

	void ReadJoystick()
	{
		joystickVertical = Input.GetAxis ("Vertical");
        joystickHorizontal = Input.GetAxis("HorizontalJoystick");
    }

    void ReadWheel()
    {
        float input = Input.GetAxis("Horizontal");
        float output = 0;
        if (input <= 0.1f)
        {
            output = input + 0.1f;
        }
        else
        {
            output = input - 0.3f;
        }
        output *= 5;
        wheelRotation = output * 120;
    }

    void ResetValues()
    {
        wheelRotation = 0;
        leftPedal = 0;
        rightPedal = 0;
    }
}
