﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCube1 : MonoBehaviour
{

    //public ParticleSystem.EmissionModule mainEm;
    //public ParticleSystem.EmissionModule secondaryEm;

    public ParticleSystem[] particles;
    private ParticleSystem.EmissionModule[] emissions;

    public GameObject ControlBox;
    public GameObject sphere;
    public GameObject innerRing;
    public GameObject outerRing;
    public GameObject terrain;
    public GameObject appearingScifi;
    public BoxCollider teleportOut;

    public float sphereSolution;
    public float innerRingSolution;
    public float outerRingSolution;

    public PlayerManager playerManager;

    AudioSource rubiconSfx;
    public GameObject rubicon;

    private bool platformRotation;
    private bool boom;
    private bool boomflag;
    private bool puzzleSolved;
    private bool solved;
    private float boxFirstRotationDiff;
    private float boxSecondRotationDiff;
    private float startRotation;
    private float timer;

    private bool puzzleFixed;
    private bool sphereSolved;
    private bool innerRingSolved;
    // Use this for initialization
    void Start()
    {
        boom = true;
        emissions = new ParticleSystem.EmissionModule[] { particles[0].emission, particles[1].emission };
        rubiconSfx = rubicon.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (platformRotation && !puzzleSolved)
        {
            CheckCubeRotation();

            if (puzzleFixed)
            {
                emissions[0].rateOverTimeMultiplier = 0;
                emissions[1].rateOverTimeMultiplier = 0;
                puzzleSolved = true;
            }
            BreathingEffect(emissions[0]);
        }
        if (puzzleSolved && !solved)
            timer += Time.deltaTime;
        if (timer > 8)
        {
            appearingScifi.SetActive(true);
            terrain.SetActive(false);
        }
        if (timer > 10)
        {
            teleportOut.enabled = true;
            puzzleSolved = false;
            timer = 0;
            solved = true;
        }
    }

    void CheckCubeRotation()
    {
        if (puzzleFixed)
            return;
        if (!sphereSolved)
        {

            float rotationDiff = transform.eulerAngles.y - outerRing.transform.eulerAngles.x;
            outerRing.transform.eulerAngles = new Vector3(outerRing.transform.eulerAngles.x,
                outerRing.transform.eulerAngles.y,
                rotationDiff - boxFirstRotationDiff);
            if (outerRing.transform.eulerAngles.z > outerRingSolution - 1 && outerRing.transform.eulerAngles.z < outerRingSolution + 1)
            {
                sphereSolved = true;
                rubiconSfx.pitch = 1.2f;
                rubiconSfx.Play();
                boxFirstRotationDiff = transform.eulerAngles.y - innerRing.transform.eulerAngles.x;
                outerRing.GetComponent<Renderer>().materials[1].color = new Color(0, 200f/255f, 0, 190f/255f);
            }
        }
        if (sphereSolved && !innerRingSolved)
        {
            float rotationDiff = transform.eulerAngles.y - innerRing.transform.eulerAngles.x;
            if (rotationDiff > 180)
                rotationDiff -= 360;
            else if (rotationDiff < -180)
                rotationDiff += 360;
            innerRing.transform.eulerAngles = new Vector3(innerRing.transform.eulerAngles.x,
                innerRing.transform.eulerAngles.y,
                rotationDiff - boxFirstRotationDiff);
            if (innerRing.transform.eulerAngles.z > innerRingSolution - 1 && innerRing.transform.eulerAngles.z < innerRingSolution + 1)
            {
                innerRingSolved = true;
                rubiconSfx.pitch = 1.0f;
                rubiconSfx.Play();
                boxFirstRotationDiff = transform.eulerAngles.y - sphere.transform.eulerAngles.y;
                innerRing.GetComponent<Renderer>().materials[1].color = new Color(0, 200f / 255f, 0, 190f / 255f);
            }
        }
        if (sphereSolved && innerRingSolved && !puzzleFixed)
        {
            float rotationDiff = transform.eulerAngles.y - sphere.transform.eulerAngles.y;

            sphere.transform.eulerAngles = new Vector3(sphere.transform.eulerAngles.x,
                sphere.transform.eulerAngles.y,
                rotationDiff - boxFirstRotationDiff);
            if (sphere.transform.eulerAngles.y > sphereSolution - 1 && sphere.transform.eulerAngles.y < sphereSolution + 1)
            {
                puzzleFixed = true;
                rubiconSfx.pitch = 0.8f;
                rubiconSfx.Play();
                MusicManager.instance.secondPuzzleSolved = true;
                sphere.GetComponent<Renderer>().materials[1].color = new Color(0, 200f / 255f, 0, 190f / 255f);
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag != "Cube")
            return;
        if (!sphereSolved)
            boxFirstRotationDiff = transform.eulerAngles.y - outerRing.transform.eulerAngles.x;
        else if (!innerRingSolved)
            boxFirstRotationDiff = transform.eulerAngles.y - innerRing.transform.eulerAngles.x;
        else if (!puzzleFixed)
            boxFirstRotationDiff = transform.eulerAngles.y - sphere.transform.eulerAngles.y;
        ControlBox.GetComponent<Renderer>().material.color = Color.red;
        platformRotation = true;
        boom = true;
        boomflag = false;
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag != "Cube")
            return;
        platformRotation = false;
        ControlBox.GetComponent<Renderer>().material.color = Color.cyan;
    }

    void BreathingEffect(ParticleSystem.EmissionModule emission)
    {
        if (boom)
        {
            if (!boomflag)
                emission.rateOverTimeMultiplier = emission.rateOverTimeMultiplier * 1.004f;
            else
                emission.rateOverTimeMultiplier = emission.rateOverTimeMultiplier / 1.01f;

            if (emission.rateOverTimeMultiplier > 1000)
            {
                boomflag = true;
            }
            else if (emission.rateOverTimeMultiplier < 20)
                boomflag = false;
        }
    }
}
