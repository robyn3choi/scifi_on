﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkEffect : MonoBehaviour {
    public float frequency;
    //public GameObject spaceship;
    public AudioSource beep;

    private Renderer renderer;
    private bool isParking;
    public AutoPark autoParkController;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
        //autoParkController = spaceship.GetComponent<AutoPark>();
    }

    // Use this for initialization
    void Start () {
        Blink();
	}
	
	// Update is called once per frame
	void Update () {
        isParking = autoParkController.parking;
	}

    void ToggleRenderer()
    {
        renderer.enabled = !renderer.enabled && isParking ? true : false;
    }

    void Beep() {
        if (isParking)
        {
            beep.Play();
        }
    }
    
    void Blink()
    {
        InvokeRepeating("ToggleRenderer", 0f, frequency);
        InvokeRepeating("Beep", 0f, frequency * 2);
    }
}
