using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorController : MonoBehaviour {

    public GameObject cameraRig;

    public AudioSource elevatorSfx;
    public AudioSource doorCloseSfx;
    public AudioSource doorOpenSfx;

    [Header("Drag the Door object here...")]
    public Transform door;
    private Vector3 closedDoorPosition;

    [Header("Drag two stops here...")]
    public Transform topStop;
    public Transform bottomStop;

    [Header("Movement Duration")]
    public float moveDuration;

    [Header("Door Close/Open Duration")]
    public float doorDuration;

    private bool isLocked;
    private bool isOnTop;

    private bool tmp_doorOpening = false;
    private Vector3 cameraPosition;

    [HideInInspector]
    public bool doorOpening
    {
        get { return tmp_doorOpening; }
        set
        {
            if (tmp_doorOpening == value) return;
            tmp_doorOpening = value;
            if (OnVariableChange != null)
                OnVariableChange(tmp_doorOpening);
        }
    }

    // Event called when the doorOpening variable is changed
    public delegate void OnVariableChangeDelegate(bool newVal);
    public event OnVariableChangeDelegate OnVariableChange;

    void VariableChangeHandle(bool newValue)
    {
        // Call coroutine and play audio
        if(newValue)
        {
            OpenDoor();
        }
        else
        {
            CloseDoor();
        }
    }

    void Awake()
    {
        closedDoorPosition = door.localPosition;
        OnVariableChange += VariableChangeHandle;
    }

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKey(KeyCode.Q) && !isLocked)
        {
            doorOpening = !doorOpening;
            cameraRig.transform.parent = transform;
            if (cameraPosition == Vector3.zero)
                cameraPosition = cameraRig.transform.localPosition;
        }

        if(Input.GetKey(KeyCode.W) && !isLocked)
        {
            if (!isOnTop)
            {
                GoTop();
                
            }
            else
                GoBottom();
        }
    }

    void GoTop()
    {
        StartCoroutine(MoveToPosition(transform, topStop.localPosition, moveDuration, false));
        isOnTop = true;
    }

    void GoBottom()
    {
        StartCoroutine(MoveToPosition(transform, bottomStop.localPosition, moveDuration, false));
        isOnTop = false;
    }

	IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove, bool onlyDoor)
    {
        isLocked = true;
        var currentPos = transform.localPosition;
        var t = 0f;
        while (t < 1)
        {        
            t += Time.deltaTime / timeToMove;
            transform.localPosition = Vector3.Lerp(currentPos, position, t);
            if (!elevatorSfx.isPlaying && !onlyDoor)
            {
                elevatorSfx.Play();
            }
            else if (elevatorSfx.isPlaying && transform.localPosition == position)
            {   
                elevatorSfx.Stop();
                if (isOnTop)
                {
                    cameraRig.transform.localPosition = new Vector3(cameraPosition.x, cameraPosition.y + 0.06f, cameraPosition.z);
                }
                else
                    cameraRig.transform.localPosition = cameraPosition;
            }
            yield return null;
        }
        isLocked = false;
    }

    void OpenDoor()
    {
        doorOpenSfx.Play();
        Vector3 openPosition = new Vector3(closedDoorPosition.x - door.lossyScale.x * 2, closedDoorPosition.y, closedDoorPosition.z);
        StartCoroutine(MoveToPosition(door, openPosition, doorDuration, true));
    }

    void CloseDoor()
    {
        doorCloseSfx.Play();
        StartCoroutine(MoveToPosition(door, closedDoorPosition, doorDuration, true));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isLocked)
            doorOpening = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && !isLocked)
            doorOpening = false;
    }
}
