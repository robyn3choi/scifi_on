﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AffordanceManager : MonoBehaviour
{
    public GameObject[] AffordanceObjects;
    private bool[] affordanceObjectStates;

    public Material AffordanceMaterial;
    public Material RegularMaterial;

    public GameObject ArrowAffordanceObject;
    public GameObject Arrow;
    public Camera Camera;

    private bool arrowActive;

    // Use this for initialization
    void Start () {
		affordanceObjectStates = new bool[AffordanceObjects.Length];
        Arrow.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	    KeyInputs();
        if (arrowActive)
	        AffordanceArrow();
	}

    void ChangeMaterial(int index)
    {
        if (index >= AffordanceObjects.Length)
            return;
        AffordanceObjects[index].GetComponent<Renderer>().material = !affordanceObjectStates[index] ? AffordanceMaterial : RegularMaterial;
        affordanceObjectStates[index] = !affordanceObjectStates[index];
    }

    void KeyInputs()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            ChangeMaterial(0);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            ChangeMaterial(1);
        if (Input.GetKeyDown(KeyCode.Alpha3))
            ChangeMaterial(2);
        if (Input.GetKeyDown(KeyCode.Alpha4))
            ChangeMaterial(3);
        if (Input.GetKeyDown(KeyCode.Alpha5))
            ChangeMaterial(4);
        if (Input.GetKeyDown(KeyCode.Alpha6))
            ChangeMaterial(5);
        if (Input.GetKeyDown(KeyCode.Alpha7))
            ChangeMaterial(6);
        if (Input.GetKeyDown(KeyCode.Alpha8))
            ChangeMaterial(7);
        if (Input.GetKeyDown(KeyCode.Alpha9))
            ChangeMaterial(8);
        if (Input.GetKeyDown(KeyCode.Alpha0))
            ChangeMaterial(9);
        if (Input.GetKeyDown(KeyCode.X))
        {
            arrowActive = !arrowActive;
            Arrow.SetActive(arrowActive);
        }

    }

    private float arrowYOffset = 100;
    private float speed = 50;
    private bool arrowUp;

    void AffordanceArrow()
    {
        if (ArrowAffordanceObject == null)
            return;
        Vector3 screenpos = Camera.WorldToScreenPoint(ArrowAffordanceObject.transform.position);

        if (screenpos.z > 0 &&
            screenpos.x > -100 && screenpos.x < Screen.width + 100 &&
            screenpos.y > 0 && screenpos.y < Screen.height - 0)
            DrawArrowOnObject(screenpos, true);
        else
        {
            if (screenpos.z > 0)
                screenpos *= -1;

            Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
            screenpos -= screenCenter;

            float angle = (Mathf.Atan2(screenpos.y, screenpos.x)) - 90 * Mathf.Deg2Rad;
            float m = -(screenpos.y / screenpos.x);

            Vector3 screenbounds = screenCenter * 0.9f;
            screenpos = screenpos.y > 0 ? new Vector3(screenbounds.y / m, screenbounds.y, 0) : new Vector3(-screenbounds.y / m, -screenbounds.y, 0);

            if (screenpos.x > screenbounds.x)
                screenpos = new Vector3(screenbounds.x, screenbounds.x * m, 0);
            else if (screenpos.x < -screenbounds.x)
                screenpos = new Vector3(-screenbounds.x, -screenbounds.x * m, 0);

            screenpos += screenCenter;
            Arrow.transform.position = screenpos;
            Arrow.transform.rotation = Quaternion.Euler(0, 0, -angle * Mathf.Rad2Deg);
        }
    }

    void DrawArrowOnObject(Vector3 screenpos, bool moving)
    {
        if (moving)
        {
            if (!arrowUp)
                arrowYOffset += Time.deltaTime * speed;
            else
                arrowYOffset -= Time.deltaTime * speed;
        }
        Arrow.transform.position = new Vector3(screenpos.x + 100, screenpos.y + arrowYOffset, 0);
        Arrow.transform.eulerAngles = new Vector3(0, 0, 180);

        if (!moving) return;
        if (arrowYOffset > 110 || arrowYOffset < 90)
            arrowUp = !arrowUp;
    }
}
