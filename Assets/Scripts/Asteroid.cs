﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
    public AudioSource collisionSound;

	// Use this for initialization
	void Start () {
	   	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Spaceship") {
            collisionSound.pitch = Random.Range(0.8f, 1.2f);
            collisionSound.Play();
        }
    }
}
