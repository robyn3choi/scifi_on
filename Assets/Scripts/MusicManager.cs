﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour {

    public AudioSource intros;
    public AudioSource loops;

    public AudioClip[] Audioclips;

    public bool firstPuzzleSolved = false;
    public bool secondPuzzleSolved = false;
    public bool RebelEndingChosen = false;
    public bool SergeantEndingChosen = false;

    bool EndingChosen = false;

    AudioClip MDStartLoop;
    AudioClip MD1StartIntro;
    AudioClip MD1End;
    AudioClip MD2StartIntro;
    AudioClip MD2End;
    AudioClip EndingSergeantIntro;
    AudioClip EndingSegeantLoop;
    AudioClip EndingRebel;

    public bool musicPlaying = false;

    bool MD1EndPlayed = false;
    bool MD2EndPlayed = false;

    bool looping = false;

    public static MusicManager instance = null;     //Allows other scripts to call functions from MusicManager.

    void Awake() {
        // singleton pattern
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy (gameObject);
        }
        DontDestroyOnLoad (gameObject);
    }

	void Start () {
        MDStartLoop = Audioclips[0];
        MD1StartIntro = Audioclips[1];
        MD1End = Audioclips[2];
        MD2StartIntro = Audioclips[3];
        MD2End = Audioclips[4];
        EndingSergeantIntro = Audioclips[5];
        EndingSegeantLoop = Audioclips[6];
        EndingRebel = Audioclips[7];
	}
	
	// Update is called once per frame
	void Update () {
        if (!intros.isPlaying && !looping && musicPlaying)
        {
            loops.Play();
            looping = true;
        }

        if (firstPuzzleSolved && !MD1EndPlayed)
        {
            loops.Stop();
            intros.clip = MD1End;
            intros.Play();
            MD1EndPlayed = true;
        }

        if (secondPuzzleSolved && !MD2EndPlayed)
        {
            loops.Stop();
            intros.clip = MD2End;
            intros.Play();
            MD2EndPlayed = true;
        }

        if (RebelEndingChosen && !EndingChosen) {
            loops.clip = EndingRebel;
            loops.Play();
            EndingChosen = true;
        }

        if (SergeantEndingChosen && !EndingChosen) {
            intros.clip = EndingSergeantIntro;
            loops.clip = EndingSegeantLoop;
            intros.Play();
            EndingChosen = true;
            musicPlaying = true;
        }

        if (Input.GetKeyDown("["))
        {
            RebelEndingChosen = true;
        }
        if (Input.GetKeyDown("]"))
        {
            SergeantEndingChosen = true;
        }
        if (Input.GetKeyDown("-"))
        {
            intros.Stop();
            loops.Stop();
        }

    }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("MD1"))
        {
            intros.clip = MD1StartIntro;
            loops.clip = MDStartLoop;
            intros.Play();
            musicPlaying = true;
        }
        else if (other.CompareTag("MD2")) {
                intros.clip = MD2StartIntro;
                loops.clip = MDStartLoop;
                intros.Play(); 
                musicPlaying = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MD1") || other.CompareTag("MD2")) {
            if (loops.isPlaying)
            {
                StartCoroutine(FadeOut(loops, 2f));
            }
            else if (intros.isPlaying)
            {
                StartCoroutine(FadeOut(intros, 2f));
            }
            musicPlaying = false;
            looping = false;
        }
    }


    public IEnumerator FadeOut (AudioSource audio, float FadeTime) {
        float startVolume = audio.volume;

        while (audio.volume > 0) {
            audio.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audio.Stop ();
        audio.volume = startVolume;
    }

}