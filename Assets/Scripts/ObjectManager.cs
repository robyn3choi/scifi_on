﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour {

    public PlayerManager playerManager;

    private bool trigger;
    private int teleportLocation;
    private float timer;
   

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (trigger)
            timer += Time.deltaTime;
        if (timer > 2)
        {
            timer = 0;
            trigger = false;
            if (teleportLocation == 1)
                playerManager.TeleportToLush();
            else if (teleportLocation == 2)
                playerManager.TeleportToIce();
            else if (teleportLocation == 3)
                playerManager.TeleportBack();
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "IceTrigger")
        {
            teleportLocation = 1;
            trigger = true;
            collider.enabled = false;
        }
        if (collider.tag == "LushTrigger")
        {
            teleportLocation = 2;
            trigger = true;
            collider.enabled = false;
        }
        if (collider.tag == "OutTrigger")
        {
            teleportLocation = 3;
            trigger = true;
            collider.enabled = false;
        }
    }
}
