﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameObject player;
    public GameObject movementPlatform;
    public ParticleSystem particleSystem;
    public TransparencyObject greenScreen;
    public Light teleportLight;
    public Transform firstDim;
    public Transform secondDim;
    public Transform firstOutDim;
    public Transform secondOutDim;
    public BoxCollider centerParking;
    public PuzzleCube room1;
    public PuzzleCube1 room2;
    public GameObject dimensionalConverter;

    private ParticleSystem.EmissionModule particles;

    private bool teleporting;
    private bool teleport;
    private bool playerMoved;
    private float teleportTimer;
    private Transform teleportGoal;
    private bool goingDown;

    private bool firstPuzzle;
    private bool secondPuzzle;

    AudioSource teleportSound;

    // Use this for initialization
    void Start () {
        particles = particleSystem.emission;
        teleportSound = player.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.C))
            TeleportToIce();
        if (Input.GetKeyDown(KeyCode.V))
            TeleportToLush();
        if (Input.GetKeyDown(KeyCode.B))
            TeleportBack();

        if (Input.GetKeyDown(KeyCode.E))
        {
            goingDown = true;
        }
        if (goingDown && dimensionalConverter.transform.localPosition.y > 5)
        {
            dimensionalConverter.transform.position = new Vector3(
                dimensionalConverter.transform.position.x,
                dimensionalConverter.transform.position.y - Time.deltaTime * 4,
                dimensionalConverter.transform.position.z);
        }

        if (teleportGoal == firstOutDim)
            firstPuzzle = true;
        else if (teleportGoal == secondOutDim)
            secondPuzzle = true;

        if (firstPuzzle && secondPuzzle && !centerParking.enabled)
            centerParking.enabled = true;

        if (teleporting && particles.rateOverTimeMultiplier < 2000)
        {
            particles.rateOverTimeMultiplier += 1000 * Time.deltaTime;
            float percentage = particles.rateOverTimeMultiplier / 2000;
            teleportLight.intensity = percentage * 0.8f;
            greenScreen.ChangeOpacity(percentage);
        }
        else if (!teleporting && particles.rateOverTimeMultiplier > 0)
        {
            particles.rateOverTimeMultiplier -= 100;
            float percentage = particles.rateOverTimeMultiplier / 2000;
            teleportLight.intensity = percentage;
            greenScreen.ChangeOpacity(percentage);
        }
        else if (teleporting && particles.rateOverTimeMultiplier > 2000)
            teleportTimer += Time.deltaTime;
        if (teleportTimer > 1)
        {
            teleporting = false;
            teleportTimer = 0;
        }
        if (!playerMoved && teleportTimer > 0.9f)
        {
            playerMoved = true;
            player.transform.position = teleportGoal.position;
            player.transform.rotation = teleportGoal.rotation;
        }

    }

    public void SetPlayerRigParent(GameObject parent)
    {
        player.transform.parent = parent.transform;
    }

    public void DeparentPlayer()
    {
        player.transform.parent = null;
    }

    public void TeleportToIce()
    {
        teleportSound.Play();
        teleporting = true;
        playerMoved = false;
        teleportGoal = firstDim;
        player.transform.parent = null;
        room2.enabled = true;
        room1.enabled = false;
    }

    public void TeleportToLush()
    {
        teleportSound.Play();
        teleporting = true;
        playerMoved = false;
        teleportGoal = secondDim;
        player.transform.parent = null;
        room1.enabled = true;
        room2.enabled = false;
    }

    public void TeleportBack()
    {
        teleportSound.Play();
        teleporting = true;
        playerMoved = false;
        if (teleportGoal == firstDim)
        {
            teleportGoal = firstOutDim;
            player.transform.parent = null;
        }
        else if (teleportGoal == secondDim)
        {
            teleportGoal = secondOutDim;
            player.transform.parent = null;
        }
    }
}
