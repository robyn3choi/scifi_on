﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speedometer : MonoBehaviour {

	private int speedLevel = -1;

	private int tmp_speed;

	public SpaceshipAccelerator spaceshipAccelerator;

	public int speed {
		get {
			return tmp_speed;
		}
		set {
			if (tmp_speed == value)
				return;
			tmp_speed = value;
			speedText.text = tmp_speed.ToString () + " m/s";

			int level = (int) Mathf.Floor (value / maxSpeed * numberOfBars);
			if (speedLevel == level) 
				return;
			speedLevel = level;
			RenderSpeedBars (speedLevel);
		}
	}
	public float maxSpeed;

	public Transform speedBars;
	private int numberOfBars;

	private TextMesh speedText;

	// Use this for initialization
	void Start () {
		numberOfBars = speedBars.childCount;
		speedText = GetComponent<TextMesh> ();
		speedText.text = Mathf.Abs(speed).ToString () + " m/s";
	}
	
	// Update is called once per frame
	void Update () {
		speed = (int)spaceshipAccelerator.speed;
		//Debug.Log (speedLevel);
	}

	void RenderSpeedBars(int speedLevel) {
		for (int i = 0; i < speedLevel; i++) {
			Transform speedBar = speedBars.GetChild (i);
			ChangeSpeedBarOpacity (speedBar, 1f);
		}
		for (int i = speedLevel; i < numberOfBars; i++) {
			Transform speedBar = speedBars.GetChild (i);
			ChangeSpeedBarOpacity (speedBar, 0.1f);
		}
	}

	void ChangeSpeedBarOpacity(Transform speedBar, float alpha) {
		Color tmpColor = speedBar.gameObject.GetComponent<SpriteRenderer> ().color;
		tmpColor.a = alpha;
		speedBar.gameObject.GetComponent<SpriteRenderer> ().color = tmpColor;
	}


}
