﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPosition : MonoBehaviour {

    public Transform camera;
    public float YOffset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(camera.position.x, camera.position.y - YOffset, camera.position.z);
    }
}
