﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour {

    public AudioSource alarmSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("0"))
        {
            if (alarmSound.isPlaying)
            {   
                alarmSound.Stop();
            }
            else
            {
                alarmSound.Play();
            }
        }
	}
}
