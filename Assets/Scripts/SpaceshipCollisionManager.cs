﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipCollisionManager : MonoBehaviour {
    public GameObject warningText;
    public GameObject warningBorder;
    public float time;
    public AudioSource collisionSfx;
	// Use this for initialization
	void Start () {
        warningText.GetComponent<MeshRenderer>().enabled = false;
        warningBorder.GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Rock")
        {
            StartCoroutine(showText(time));
            collisionSfx.pitch = Random.Range(0.8f, 1.2f);
            collisionSfx.Play();
        }
    }

    IEnumerator showText(float time)
    {
        warningText.GetComponent<MeshRenderer>().enabled = true;
        warningBorder.GetComponent<SpriteRenderer>().enabled = true;
        yield return new WaitForSeconds(time);
        warningText.GetComponent<MeshRenderer>().enabled = false;
        warningBorder.GetComponent<SpriteRenderer>().enabled = false;
    }
}
