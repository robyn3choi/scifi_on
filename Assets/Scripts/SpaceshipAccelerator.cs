﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipAccelerator : MonoBehaviour {

    private const int MAXSPEED = 250;

	public ControllerManager controllerManager;
    public AutoPark autoPark;
	public Speedometer speedometer;
	public float curMaxSpeed = 100;
	public float accelerationMultiplier = 0.03f;
	public float decelerationMultiplier = 0.01f;

    bool nearBuilding = false;

	public float speed;

    public GameObject armature;
    AudioSource spaceshipHum;
    public float maxPitch = 2.8f;

    float currentVelocity = 0; //for smoothdamp

	// Use this for initialization
	void Start () {
        spaceshipHum = armature.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //PedalControls ();
        //KeyboardControls ();
        //if (controllerManager.JoystickTrigger)
           // JoystickControls();
        if (autoPark.parking || autoPark.parked)
        {
            speed = 0;
            spaceshipHum.pitch = Mathf.SmoothDamp(spaceshipHum.pitch, 1f, ref currentVelocity, 0.8f);
            return;
        }

        WheelControls();
        curMaxSpeed = MAXSPEED - controllerManager.wheelRotation * 2;
        if (speed > curMaxSpeed)
            speed = MAXSPEED - controllerManager.wheelRotation * 2;
        if (speed < 0)
            speed = 0;

        if (!nearBuilding)
		    transform.Translate (-Vector3.forward * speed * Time.deltaTime * 0.2f);
	}

	void KeyboardControls()
	{
        if (Input.GetKey(KeyCode.Space) && speed < curMaxSpeed)
			speed += 1f + speed * accelerationMultiplier;
       
		if (Input.GetKey (KeyCode.S) && speed > 0)
			speed -= 0.2f + speed * decelerationMultiplier;

	}

	void PedalControls()
	{
		if (controllerManager.leftPedal > 0 && speed < curMaxSpeed) {
			speed += 0.15f + speed * accelerationMultiplier * controllerManager.leftPedal;
		} else if (controllerManager.rightPedal > 0 && speed > 0)
			speed -= 0.2f + controllerManager.rightPedal * speed * decelerationMultiplier;
	}

    void JoystickControls()
    {
        speed += (0.15f + speed * accelerationMultiplier) * controllerManager.wheelRotation;

    }

    void WheelControls()
    {
        speed += (0.15f + speed * accelerationMultiplier) * -(controllerManager.wheelRotation / 120f);

        if (controllerManager.wheelRotation < 0)
        {
            // spaceship rev sound
            if (spaceshipHum.pitch < maxPitch)
            {
                float unRoundedPitch = (((controllerManager.wheelRotation - 0) * (maxPitch - 1f)) * (-1) / 120) + 1;
                spaceshipHum.pitch = Mathf.Round(unRoundedPitch * 100f) / 100f;
            }
        }

        // spaceship going back to idle
        else
        {
            spaceshipHum.pitch = Mathf.SmoothDamp(spaceshipHum.pitch, 1f, ref currentVelocity, 0.8f);
        }
    }

//    void OnTriggerEnter(Collider other) {
//        if (other.CompareTag("SpaceStationCollider")) {
//            nearBuilding = true;
//            speed = 0;
//        }
//    }
//
//    void OnTriggerExit(Collider other) {
//        if (other.CompareTag("SpaceStationCollider")) {
//            nearBuilding = false;
//        }
//    }
//
//    void SlowDown() {
//        speed = 0;
//    }
}

