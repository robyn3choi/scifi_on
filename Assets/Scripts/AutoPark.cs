﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPark : MonoBehaviour {

    public float groundLevel = 54.5f;
    public float spaceshipLevel;
    public bool parking;
    public bool parked;
    public PlayerManager playerManager;
    public ParticleSystem field;
    public ParticleSystem secondField;
    public GameObject elevatorForcefield;

    private ParticleSystem.EmissionModule fieldEmission;
    private ParticleSystem.EmissionModule secondFieldEmission;
    private Transform autoParkPosition;
	private float autoParkSpeed;
    private float rotationCounter;
    private float rotationSpeed = 0.2f;
    private float platformTimer;
	private bool firstPark = true;
	private bool platformDown;
    private bool waitingForPlatformUp;
    private bool rotateSpaceship;
    private AudioSource forcefieldSfx;
    private AudioSource elevatorSfx;

	// Use this for initialization
	void Start () {
        fieldEmission = field.emission;
        secondFieldEmission = secondField.emission;
        autoParkSpeed = 10;
        forcefieldSfx = elevatorForcefield.GetComponents<AudioSource>()[0];
       //elevatorSfx = elevatorForcefield.GetComponents<AudioSource>()[1];
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.A))
        {
            waitingForPlatformUp = false;
            playerManager.player.transform.parent = playerManager.movementPlatform.transform;
            field.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Mesh;
            secondField.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Mesh;
            if (parked)
            {
                forcefieldSfx.Play();
                //elevatorSfx.Play();
            }
        }
        ParkSpaceship();
        //if (rotateSpaceship)
            //RotateSpaceship();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "AutoPark")
        {
            autoParkPosition = collider.gameObject.transform;
            parking = true;
            autoParkSpeed = 5;
            collider.enabled = false;
            //Debug.Log("trigger");
        }
        if (collider.tag == "FirstPark")
        {
            autoParkPosition = collider.gameObject.transform;
            parking = true;
            autoParkSpeed = 60;
        }
    }

	void MovePlayerPlatform()
	{
        //Debug.Log(playerManager.movementPlatform.transform.position.y);
		switch (platformDown) {
			case false:
                if (playerManager.movementPlatform.transform.localPosition.y > groundLevel)
                    playerManager.movementPlatform.transform.position = new Vector3(
                        playerManager.movementPlatform.transform.position.x,
                        playerManager.movementPlatform.transform.position.y - Time.deltaTime,
                        playerManager.movementPlatform.transform.position.z);
                else
                {
                    platformDown = true;
                    waitingForPlatformUp = true;
                    platformTimer = 0;
                    field.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.None;
                    secondField.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.None;
                    forcefieldSfx.Stop();
                   // elevatorSfx.Stop();
                }
                break;
			case true:
                if (playerManager.movementPlatform.transform.localPosition.y < spaceshipLevel)
                    playerManager.movementPlatform.transform.localPosition = 
                        Vector3.MoveTowards(playerManager.movementPlatform.transform.localPosition, 
                        new Vector3(0.00313082f, -0.472f, 8.509f), 
                        Time.deltaTime);
                else
                {
                    platformDown = false;
                    parked = false;
                    parking = false;
                    rotateSpaceship = true;
                    platformTimer = 0;
                    field.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.None;
                    secondField.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.None;
                    forcefieldSfx.Stop();
                   // elevatorSfx.Stop();
                }
				break;
		}
	}

    void RotateSpaceship()
    {
        rotationCounter += Time.deltaTime;
        if (rotationCounter < 90)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + Time.deltaTime, transform.eulerAngles.z);
        else
        {
            parked = false;
            rotationCounter = 0;
            rotateSpaceship = false;
        }     
    }

    void ParkSpaceship()
    {
        if (parking)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(autoParkPosition.position.x,
                autoParkPosition.position.y,
                autoParkPosition.position.z),
                autoParkSpeed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, autoParkPosition.rotation, rotationSpeed);
        }
        if (parking && !parked && Mathf.Abs(transform.position.x - autoParkPosition.position.x) < 2f &&
            Mathf.Abs(transform.position.y - autoParkPosition.position.y) < 2f &&
            Mathf.Abs(transform.position.z - autoParkPosition.position.z) < 2f &&
            transform.rotation == autoParkPosition.rotation)
        {
            parking = false;
            transform.position = autoParkPosition.position;
            parked = true;
            playerManager.SetPlayerRigParent(playerManager.movementPlatform);
            transform.parent = playerManager.movementPlatform.transform;
            field.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Mesh;
            secondField.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Mesh;
            forcefieldSfx.Play();
           // elevatorSfx.Play();
            //Debug.Log("parked");
        }
        if (parked && !waitingForPlatformUp)
        {
            platformTimer += Time.deltaTime;
            fieldEmission.rateOverTimeMultiplier = platformTimer * 4;
            secondFieldEmission.rateOverTimeMultiplier = platformTimer * 3;
            if (platformTimer > 2)
                MovePlayerPlatform();
        }
    }
}
