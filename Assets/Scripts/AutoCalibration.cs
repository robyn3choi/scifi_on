﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCalibration : MonoBehaviour {

    public Transform controller;
    private Vector3 controllerInitialPosition;

    private bool isCalibrating = false;

    // Use this for initialization
    void Start () {
        StartCoroutine(InitialCalibrate());
    }

    // Update is called once per frame
    void Update() {

        if (SteamVR.calibrating)
        {
            isCalibrating = true;
            controllerInitialPosition = controller.position;
        }
        if (isCalibrating && !SteamVR.calibrating)
        {
            isCalibrating = false;
            RuntimeCalibrate();
        }

    }

    IEnumerator InitialCalibrate()
    {
        yield return new WaitForSeconds(1f);
        transform.parent = controller;
        transform.localPosition = Vector3.zero;
        //yield return new WaitForSeconds(1f);
        while (SteamVR.calibrating) { }
        transform.parent = null;
    }

    void RuntimeCalibrate()
    {
        Vector3 currentPosition = controller.position;
        Vector3 distance = currentPosition - controllerInitialPosition;
        transform.Translate(distance);
    }

}
