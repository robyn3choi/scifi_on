﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputType
{
    verticalJoystick, horizontalJoystick, wheel
}

public enum Axis
{
    x, y, z
}

public class JoyStickAnimationController : MonoBehaviour {
    public ControllerManager controllerManager;
    public InputType inputType;
    public Axis rotationAxis;

    public float minAngle;
    public float maxAngle;

    public Axis pivotAxis;
    public float pivotPoint;

    private float inputValue;
	// Use this for initialization
	void Start () {
        ChangePivotPoint(pivotAxis, pivotPoint);
	}
	
	// Update is called once per frame
	void Update () {
        switch (inputType) {
            case InputType.horizontalJoystick:
                inputValue = controllerManager.joystickHorizontal;
                break;
            case InputType.verticalJoystick:
                inputValue = controllerManager.joystickVertical;
                break;
            case InputType.wheel:
                inputValue = controllerManager.wheelRotation / 120;
                break;
            default:
                break;
        }

       // Debug.Log(inputValue);
        
        var angle = Normalize(inputValue, minAngle, maxAngle);
        RotateAround(rotationAxis, angle);
	}

    void RotateAround(Axis axis, float angle)
    {
        switch(axis)
        {
            case Axis.x:
                transform.parent.localRotation = Quaternion.AngleAxis(angle, Vector3.left);
                break;
            case Axis.y:
                transform.parent.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
                break;
            case Axis.z:
                transform.parent.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
                break;
            default:
                break;
        }
    }

    float Normalize(float value, float min, float max)
    {
        float normalizedValue = (value + 1) / 2 * (max - min) + min;
        return normalizedValue;
    }

    void ChangePivotPoint(Axis axis, float pivotPoint)
    {
        var parent = new GameObject("Parent");
        parent.transform.position = transform.position;
        parent.transform.parent = transform.parent;
        parent.transform.localRotation = Quaternion.Euler(Vector3.zero);
        transform.parent = parent.transform;
        switch (axis)
        {
            case Axis.x:
                transform.localPosition = new Vector3(transform.localPosition.x - pivotPoint, transform.localPosition.y, transform.localPosition.z);
                parent.transform.position = new Vector3(parent.transform.position.x + pivotPoint, parent.transform.position.y, parent.transform.position.z);
                break;
            case Axis.y:
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - pivotPoint, transform.localPosition.z);
                parent.transform.position = new Vector3(parent.transform.position.x, parent.transform.position.y + pivotPoint, parent.transform.position.z);
                break;
            case Axis.z:
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - pivotPoint);
                parent.transform.position = new Vector3(parent.transform.position.x, parent.transform.position.y, parent.transform.position.z + pivotPoint);
                break;
        }
    }
} 
