﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    left, right, none
}

public class AffordanceMonitor: MonoBehaviour {
    public Transform target;
    public GameObject spaceship;

    #region Arrows Objects
    public GameObject arrowLeft;
    public GameObject arrowRight;
    #endregion

    #region Arrows Renderers
    private SpriteRenderer arrowLeftRenderer;
    private SpriteRenderer arrowRightRenderer;
    #endregion

    private Direction direction;
    private bool isTargetSeen;

    private Camera mainCamera;
    private AutoPark autoParkScript;
    private bool isParking;

    void Awake()
    {
        mainCamera = Camera.main;
        autoParkScript = spaceship.GetComponent<AutoPark>();
    }

    // Use this for initialization
    void Start () {
        arrowLeftRenderer = arrowLeft.GetComponent<SpriteRenderer>();
        arrowRightRenderer = arrowRight.GetComponent<SpriteRenderer>();

        DisableArrowRenderers();
    }
	
	// Update is called once per frame
	void Update () {
        CheckIfTargetIsSeen();
        isParking = autoParkScript.parking;
        direction = GetDirection();

		if(!isTargetSeen && !isParking)
        {
            switch(direction)
            {
                case Direction.left:
                    DisableArrowRenderers();
                    arrowLeftRenderer.enabled = true;
                    break;
                case Direction.right:
                    DisableArrowRenderers();
                    arrowRightRenderer.enabled = true;
                    break;
                default:
                    DisableArrowRenderers();
                    break;
            }
        } 
        else
            DisableArrowRenderers();
    }

    Direction GetDirection()
    {
        float distanceX = (transform.position.x - target.position.x) * transform.forward.normalized.z;

        if (distanceX > 0)
            return Direction.left;
        else if (distanceX < 0)
            return Direction.right;
        else
            return Direction.none;
    }

    void CheckIfTargetIsSeen()
    {
        Vector3 screenPoint = mainCamera.WorldToViewportPoint(target.position);
        isTargetSeen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
    }

    void DisableArrowRenderers()
    {
        arrowLeftRenderer.enabled = false;
        arrowRightRenderer.enabled = false;
    }
}
