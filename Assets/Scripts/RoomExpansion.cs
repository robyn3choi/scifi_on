﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomExpansion : MonoBehaviour {

	public float expandDistance;
	public Vector3 expandDirection;

	public Transform wallsToMoveIfExpand;
	public Transform wallsToScale;
	public Transform wallsToMoveIfCompress;

	public Camera mainCamera;

	public Transform expandTrigger;
	public Transform compressTrigger;

	public float cameraDetectRange;

	// Variable to control expansion
	private bool tmp_isExpandTriggerSeen = false;
	private bool isExpandTriggerSeen {
		get {
			return tmp_isExpandTriggerSeen;
		}
		set {
			if(tmp_isExpandTriggerSeen == value) 
				return;
			tmp_isExpandTriggerSeen = value;
			if(!isExpanded) 
				ExpandRoom ();
		}
	}
	private bool isExpanded = false;

	// Variable to control compression
	private bool tmp_isCompressTriggerSeen = false;
	private bool isCompressTriggerSeen {
		get {
			return tmp_isCompressTriggerSeen;
		}
		set {
			if(tmp_isCompressTriggerSeen == value) 
				return;
			tmp_isCompressTriggerSeen = value;
			if(isExpanded) 
				CompressRoom ();
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (IsCameraSeeing (expandTrigger, cameraDetectRange)) {
			isExpandTriggerSeen = true;
		} else
			isExpandTriggerSeen = false;

		if (IsCameraSeeing (compressTrigger, cameraDetectRange))
			isCompressTriggerSeen = true;
		else
			isCompressTriggerSeen = false;
	}

	//================================================================================
	// THE ROOM IS EXPANDED IF AN EXPAND TRIGGER OBJECT IS SEEN BY THE CAMERA
	//================================================================================
	void ExpandRoom() {
		Debug.Log ("Expanding");

		// Move left walls to the left
		wallsToMoveIfExpand.Translate (expandDirection * expandDistance);

		// Scale & move right walls 
		Scale(wallsToScale, expandDistance, expandDirection);

		isExpanded = true;
	}

	void Scale(Transform transform, float amount, Vector3 moveDirection) {
		if (transform.childCount == 0) {
			transform.localScale += new Vector3 (amount, 0, 0);
			transform.Translate (moveDirection * amount / 2);
			return;
		}
		foreach(Transform childTransform in transform) {
			Scale (childTransform, amount, moveDirection);
		}
	}

	//================================================================================
	// THE ROOM IS COMPRESSED IF A COMPRESS TRIGGER OBJECT IS SEEN BY THE CAMERA
	//================================================================================
	void CompressRoom() {
		Debug.Log ("Compressing");

		// Move right walls to the left
		wallsToMoveIfCompress.Translate(expandDirection * expandDistance);

		// Scale & move right walls to opposite direction
		Scale(wallsToScale, -expandDistance, -expandDirection);

		isExpanded = false;

        //CloneRoom();
	}

	bool IsCameraSeeing(Transform obj, float range) {
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);

		bool isSeeing = GeometryUtility.TestPlanesAABB(planes , obj.GetComponent<Collider>().bounds); 
		bool isNear = Vector3.Distance (mainCamera.transform.position, obj.position) < range;

		return (isSeeing && isNear);
	}

    void CloneRoom()
    {
        Instantiate(gameObject, new Vector3(transform.position.x, transform.position.y, transform.position.z + 5f), Quaternion.identity);
    }
}
