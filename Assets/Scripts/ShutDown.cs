﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShutDown : MonoBehaviour {

	public AudioSource whirl;
	public AudioSource shutDown;

    public GameObject particle1;
    public GameObject particle2;

	public GameObject alarm1;
	public GameObject alarm2;

    public ParticleSystem dimensionalConverter;
    public ParticleSystem dimensionalConverterGlow;
    public ParticleSystem dimensionalConverterRings;
    public ParticleSystem dimensionalConverterParticles;

    private ParticleSystem.EmissionModule dimensionalEmitter;
    private ParticleSystem.EmissionModule dimensionalEmitterGlow;
    private ParticleSystem.EmissionModule dimensionalEmitterRings;
    private ParticleSystem.EmissionModule dimensionalEmitterParticles;

    public Light powerSpotlight;

    private bool shutdown;

    // Use this for initialization
    void Start () {
        if (dimensionalConverter != null)
            dimensionalEmitter = dimensionalConverter.emission;
        if (dimensionalConverterGlow != null)
            dimensionalEmitterGlow = dimensionalConverterGlow.emission;
        if (dimensionalConverterRings != null)
            dimensionalEmitterRings = dimensionalConverterRings.emission;
        if (dimensionalConverterParticles != null)
            dimensionalEmitterParticles = dimensionalConverterParticles.emission;
    }

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("="))
		{
			print ("shutdown");
			whirl.Stop();
			shutDown.Play();
			alarm1.GetComponent<AudioSource>().Stop();
			alarm2.GetComponent<AudioSource>().Stop();
            if (particle1 != null)
            particle1.GetComponent<ParticleSystem>().Stop();
            if (particle1 != null)
                particle2.GetComponent<ParticleSystem>().Stop();
            shutdown = true;
        }
        if (shutdown && powerSpotlight != null && powerSpotlight.intensity > 0)
            powerSpotlight.intensity -= Time.deltaTime * 3;
        if (dimensionalConverter != null && shutdown && dimensionalEmitter.rateOverTimeMultiplier > 0)
            dimensionalEmitter.rateOverTimeMultiplier -= Time.deltaTime * 2;
        if (dimensionalConverterGlow != null && shutdown && dimensionalEmitterGlow.rateOverTimeMultiplier > 0)
            dimensionalEmitterGlow.rateOverTimeMultiplier -= Time.deltaTime * 8;
        if (dimensionalConverterRings != null && shutdown && dimensionalEmitterRings.rateOverTimeMultiplier > 0)
            dimensionalEmitterRings.rateOverTimeMultiplier -= Time.deltaTime * 2;
        if (dimensionalConverterParticles != null && shutdown && dimensionalEmitterParticles.rateOverTimeMultiplier > 0)
            dimensionalEmitterParticles.rateOverTimeMultiplier -= Time.deltaTime * 8;


    }
}
