﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRandomizer : MonoBehaviour {
    public int numberOfAnimations;

	// Use this for initialization
	void Start () {
	    foreach(Transform child in transform)
        {
            int index = Random.Range(0, numberOfAnimations - 1);

            Animator anim = child.GetComponent<Animator>();
            anim.SetInteger("random", index);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
