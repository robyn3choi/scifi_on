# Description

ON is a public VR experience which is developed with Unity 5. We recommend using the same version of Unity in Windows to make sure it runs properly.

# Authors

* Hoa Nguyen
* Robyn Choi
* Frederik Nielsen

# Requirements

## Hardware Requirements

**Minimum PC Specifications (According to HTC Vive)**

* GPU: NVIDIA GeForce GTX 970 / AMD Radeon R9 290 equivalent or greater
* CPU: Intel Core i5-4590 / AMD FX 8350 equivalent or greater
* RAM: 4GB+
* USB 3.0 controller. USB 2.0 is not supported

**Our Recommended PC Specifications**

* CPU: Intel Core i7-3770
* GPU: NVIDIA GeForce GTX 1070 
* RAM: 16GB DDR4-2400 
* USB 3.0 controller. USB 2.0 is not supported

## Operating System Requirement

SteamVR currently does not support any other operating system than Windows. We recommend running the project in Windows 8.0, Windows 8.1 or Windows 10.

## External devices
In this project, these input devices are used:

* HTC Vive.
* Microsoft sidewinder precision racing wheel.
* Arduino Uno connected with some buttons.
* LEAP Motion.
    
In order to open and run the source code, please make sure that you have the same or similar devices.


# Setup instruction


**Steam VR**

After having a proper room with all VR devices setup, you can follow the instruction on [this link](https://support.steampowered.com/kb_article.php?ref=2001-UXCM-4439) to install SteamVR.

**Arduino Uno**

**LEAP Motion**

You can download and install Leap Motion Orion on [this link](https://developer.leapmotion.com/orion/#105). 
Orion includes the SDK for Leap Motion and a Unity package (which is already in the project file). Orion, however, is not available on any other OS than Windows.

**Microsoft Sidewinder precision racing wheel**

This racing wheel is an old model that is most compatible for Windows 98 and XP. On the new versions of Windows, we already wrote some lines of code that fix the input problem caused by Microsoft Sidewinder. 
If you have another racing wheel that does not cause the same problem, just simply comment those lines of code in RacingWheel.cs file. 


