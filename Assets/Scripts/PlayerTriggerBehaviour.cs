﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerTriggerBehaviour : MonoBehaviour
    { 
        public GameObject car;
        private Animator carAnimation;
        public bool firstTriggerActivated;
        private bool startNewAnimation;
        private float timer;
        private bool started;
        // Use this for initialization
        void Start ()
        {
            firstTriggerActivated = false;
            carAnimation = GetComponentInParent<Animator>();

        }
	
        // Update is called once per frame
        void Update () {

            //if (startNewAnimation)
            //{
            //    timer += Time.deltaTime;
            //}

            //if (timer > 3)
            //{
            //    timer = 0;
            //    startNewAnimation = false;
            //  carAnimation.SetTrigger("FlyBack");
            //}

   //         if (Input.GetKeyDown(KeyCode.Space) && !started)
   //         {
   //             started = true;
   //             carAnimation.SetTrigger("FlyToRoof");
   //         }

			//if (Input.GetKeyDown(KeyCode.Q))
			//{
			//	GameObject.FindGameObjectWithTag ("Dummy").GetComponent<MeshRenderer> ().enabled = !GameObject.FindGameObjectWithTag ("Dummy").GetComponent<MeshRenderer> ().enabled;
			//	GameObject curController = GameObject.FindGameObjectWithTag ("Controller");
			//	curController.GetComponent<MeshRenderer> ().enabled = !curController.GetComponent<MeshRenderer> ().enabled;

			//}

			//if (Input.GetKeyDown (KeyCode.W)) {
			//	GameObject.Find ("PlayerCollider").GetComponent<PlayerTriggerBehaviour> ().firstTriggerActivated = true;
			//}

			//if (Input.GetKeyDown (KeyCode.E)) {
			//	carAnimation.SetTrigger("FlyBack");
			//}
        }

        void OnTriggerEnter(Collider col)
        {
            if (firstTriggerActivated && col.gameObject.tag == "Player")
            {
                startNewAnimation = true;
            }
        }
    }
}
