﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransparencyObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void ChangeOpacity(float percentage)
    {
        Color color = GetComponent<Renderer>().material.color;
        color.a = percentage;
        GetComponent<Renderer>().material.color = color;
    }
}
