﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RacingWheel : MonoBehaviour
{
    public float wheelRotation;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // Get the input from the racing wheel
        float input = Input.GetAxis("Horizontal");
        // Generate the output in range -1 to 1
        float output = 0;
        if (input <= 0.1f)
        {
            output = input + 0.1f;
        }
        else
        {
            output = input - 0.3f;
        }
        output *= 5;
	    wheelRotation = output*120;

        transform.localEulerAngles = new Vector3(-30, 0, wheelRotation);
	}
}
